# README

Projeto de teste para a vaga de estágio de desenvolvimento.

> **Obs:**
>
> 1.  Projeto pode ser executado pelo XAMP, WAMP, MAMP ou qualquer servidor que contenha PHP + MySQL.
> 2.  O banco de dados do projeto se encontra no arquivo teste-wordpress.sql.
> 3.  Qualquer problema com a instalação do projeto pode nos contactar que auxiliamos.

## Tarefas

---

### Página blog

- Deixar a página de acordo com o layout:
  ![Alt text](https://cdn.discordapp.com/attachments/711956822575022110/728014587869265970/unknown.png)
- Na listagem de posts, **utilizar diplay grid ou flex**.
- Adicionar o conteúdo (faltante) do post abaixo do título.

### JavaScript

- Adicionar um botão flutante no lado direito da página para que quando o usuário rolar a página para baixo ele possa clica-lo para voltar ao topo.
