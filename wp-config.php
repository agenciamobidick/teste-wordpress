<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'teste-wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'I:YRTY5rY9I&vIicVkPnnAaNj.[#32jq(d5L:Bwq+2O=X[[~@CjxLdt/&1e<1cqy' );
define( 'SECURE_AUTH_KEY',   '&8Du/ueH4VA1D0_5?CzE7:0D&]MwGPH)ma`?u)h7zUf HECDn% T,p8]:;JOr3-~' );
define( 'LOGGED_IN_KEY',     'q6B]0FOTt:o2}V%Dg!jX!@Me_SRLHy?d>]#OHzRW]=^1@v1|?UqO9[4 {Y9<cP$E' );
define( 'NONCE_KEY',         '$?`C*#fU|dd~ltLp/vC7av{w#Z[H%Mkdh4D%^8A{ 2p=f9MbIc=+SSH m`%ckExE' );
define( 'AUTH_SALT',         'yY{HeD>Be><_<FdIx0$H1LS&mu_NqF!)p$I#1<ik,Wd}WNZQ1XD&(hNT7#leL|)V' );
define( 'SECURE_AUTH_SALT',  'wNtYi;=Xl{SCtxvAn3gG&r!zFuJ(e<cIBe:7!fxe+,WEU8}1]+2NM_~sEQZ{~`=:' );
define( 'LOGGED_IN_SALT',    '59NOp@2)F-ru/gntQO3R~x`AS/zSQ;9:Kk7:PYIoB0a>c8R,m`&}ZMPnN$$ @VC+' );
define( 'NONCE_SALT',        'C{q?]3Vw*I)ycj$C.ueBk>fAVdEV%ac>{anyRRONtCdom^,WGqV<;H~?v_QkW!Z[' );
define( 'WP_CACHE_KEY_SALT', ':^#nhwtB0]?BAZM1:#/YE3ndfox;{ZhLcwjRV2Cn.E-e,dFh2pQ|ELxo7ZX!H/2T' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
