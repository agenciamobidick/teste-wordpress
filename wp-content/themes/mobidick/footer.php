</main> 
<!-- END main -->

<!-- BEGIN footer -->
<footer id="footer">
    <div class="container d-flex align-items-center justify-content-center">
        <p class="copyright">© <?php echo date_i18n( 'Y' ); ?> MOBIDICK. All rights reserved</p>
    </div>
</footer>
<!-- END footer -->

<?php wp_footer(); ?>

</body>
</html>