<!-- BEGIN sidebar -->
<aside id="sidebar">
	<ul id="widgetlist">

    <?php if ( is_active_sidebar( 'sidebar' ) ) :
        dynamic_sidebar( 'sidebar' );
    else : ?>

		<li class="widget widget_search">
			<?php get_search_form(); ?>
		</li>

		<?php wp_list_categories('use_desc_for_title=0&title_li=<p class="wtitle">'. __("Categorias", 'teste-wordpress') .'</p>');  ?>

	<?php endif; ?>

	</ul>
</aside>
<!-- END sidebar -->
