<?php get_header(); ?>

    <!-- BEGIN content -->
	<section id="content" class="content">

		<?php while ( have_posts() ) : the_post(); ?>

			<article class="post page" id="pageid-<?php the_ID(); ?>">

				<h1><?php the_title(); ?></h1>

				<div class="entry-box clearfix">
					<?php the_content(); ?>
				</div>

			</article>

		<?php endwhile; ?>
		
	</section> <!-- #content -->

	<?php get_sidebar(); ?>

<?php get_footer(); ?>