<?php get_header(); ?>

<div class="container d-flex">
    <!-- BEGIN content -->
    <section id="content" class="content">

        <?php while ( have_posts() ) : the_post(); ?>

            <article class="post page" id="pageid-<?php the_ID(); ?>">

                <div class="entry-box clearfix">
                    <?php the_content(); ?>
                </div>

            </article>

        <?php endwhile; ?>
    </section>
    <!-- END content -->
</div>

<?php get_footer(); ?>