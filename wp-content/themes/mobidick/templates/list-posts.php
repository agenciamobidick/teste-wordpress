<div class="container">
    <!-- BEGIN content -->
    <section id="content" class="content">

        <ul class="list">

        <?php while ( have_posts() ) : the_post(); ?>

            <article class="post page" id="pageid-<?php the_ID(); ?>">

                <h2><a href="<?php permalink_link()?>" class="clear_link"><?php the_title(); ?></a></h2>

            </article>

        <?php endwhile; ?>

        </ul>

        <?php the_posts_pagination( [
            'mid_size'  => 2,
            'prev_text' => __( 'Voltar', 'teste-wordpress' ),
            'next_text' => __( 'Avançar', 'textdomain' ),
        ] ); ?>

    </section> <!-- #content -->
        
    <?php get_sidebar(); ?>
</div>