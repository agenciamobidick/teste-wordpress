<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" /><![endif]-->
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

    <!-- BEGIN header -->
    <header id="header">
        <div class="container d-flex align-items-center">

            <!-- Logo -->
            <?php if( has_custom_logo() ) { ?>
                <div class="header_logo">
                    <?php the_custom_logo(); ?>
                </div>
            <?php }?>

            <!-- Título -->
            <?php if(is_home() || is_front_page()) { ?>
                <h1 class="header_titulo">
                    <a class="clear_link" href="<?php echo TESTE_WORDPRESS_SITE_URL; ?>"><?php echo TESTE_WORDPRESS_SITE_NAME; ?></a>
                </h1>
            <?php } else { ?>
                <a href="<?php echo TESTE_WORDPRESS_SITE_URL; ?>" class="header_titulo clear_link"><?php echo TESTE_WORDPRESS_SITE_NAME; ?></a>
            <?php } ?>

            <!-- Menu -->
            <?php if( has_nav_menu('primary') ) { ?>
                <?php wp_nav_menu( ['theme_location' => 'primary'] );?>
            <?php } ?>

        </div>
    </header>
    <!-- END header -->

    <!-- BEGIN main -->
    <main id="main">