<?php
/**
 * Arquivo inicial de configurão do tema
 * 
 * @author  Gabriel Carvalho
 * @since   1.0
 * @package TesteWordpress
 */

/**
 * Contantes do site
 */
// Theme
$_teste_wordpress_theme = wp_get_theme();
define( 'TESTE_WORDPRESS_THEME_NAME',                  $_teste_wordpress_theme->get('TextDomain') );
define( 'TESTE_WORDPRESS_THEME_VERSION',               $_teste_wordpress_theme->get('Version') );

// Path
define( 'TESTE_WORDPRESS_PATH_ROOT',                   get_template_directory().'/' );
    define( 'TESTE_WORDPRESS_PATH_TEMPLATES',          TESTE_WORDPRESS_PATH_ROOT.'templates/' );
    define( 'TESTE_WORDPRESS_PATH_INC',                TESTE_WORDPRESS_PATH_ROOT.'inc/' );
    define( 'TESTE_WORDPRESS_PATH_ASSETS',             TESTE_WORDPRESS_PATH_ROOT.'assets/' );
        define( 'TESTE_WORDPRESS_PATH_CSS',            TESTE_WORDPRESS_PATH_ASSETS.'css/' );
        define( 'TESTE_WORDPRESS_PATH_JS',             TESTE_WORDPRESS_PATH_ASSETS.'js/' );
        define( 'TESTE_WORDPRESS_PATH_FONTS',          TESTE_WORDPRESS_PATH_ASSETS.'fonts/' );
        define( 'TESTE_WORDPRESS_PATH_IMAGES',         TESTE_WORDPRESS_PATH_ASSETS.'images/' );

// Url
define( 'TESTE_WORDPRESS_URL_ROOT',                    get_template_directory_uri().'/' );
    define( 'TESTE_WORDPRESS_URL_TEMPLATES',           TESTE_WORDPRESS_URL_ROOT.'templates/' );
    define( 'TESTE_WORDPRESS_URL_INC',                 TESTE_WORDPRESS_URL_ROOT.'inc/' );
    define( 'TESTE_WORDPRESS_URL_ASSETS',              TESTE_WORDPRESS_URL_ROOT.'assets/' );
        define( 'TESTE_WORDPRESS_URL_CSS',             TESTE_WORDPRESS_URL_ASSETS.'css/' );
        define( 'TESTE_WORDPRESS_URL_JS',              TESTE_WORDPRESS_URL_ASSETS.'js/' );
        define( 'TESTE_WORDPRESS_URL_FONTS',           TESTE_WORDPRESS_URL_ASSETS.'fonts/' );
        define( 'TESTE_WORDPRESS_URL_IMAGES',          TESTE_WORDPRESS_URL_ASSETS.'images/' );

// Url Site
define( 'TESTE_WORDPRESS_SITE_URL',                    get_site_url() );

// Current Url Site
define( 'TESTE_WORDPRESS_CURRENT_URL_SITE',            sprintf( '%s://%s%s', $_SERVER['REQUEST_SCHEME'], $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI'] ) );

//Site Name
define( 'TESTE_WORDPRESS_SITE_NAME',                   get_bloginfo() );

/**
 * Functions
 */
require TESTE_WORDPRESS_PATH_INC.'teste-wordpress-functions.php';

/**
 * Setup, registers e supports
 */
require TESTE_WORDPRESS_PATH_INC.'class-teste-wordpress.php';