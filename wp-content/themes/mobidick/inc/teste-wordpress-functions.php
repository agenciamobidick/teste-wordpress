<?php
/**
 * Arquivo de funções do tema
 * 
 * @author  Gabriel Carvalho
 * @since   1.0
 * @package TesteWordpress
 */

if ( ! function_exists('echoVar') ) {
    /**
     * Exibe uma variável de qualquer tipo
     *
     * @author Gabriel Carvalho <gabriel.dossantoscarvalho@hotmail.com>
     * @param string $var Variável a ser exibibida
     * @param string $corTexto Hexadecimal da cor do texto a ser exibido
     * @return void
     */
    function echoVar( $var, $corTexto = "#000" )
    {
        printf( '<div style="font-family: monospace; position: relative; float: left; width: 100&percnt;; z-index: 100000; line-height: 1.2; color: %s; background: #eee; padding: 10px; font-size: 16px;"><pre>%s</pre></div>',
            $corTexto,
            print_r( $var, true )
        );
    }
}

if ( ! function_exists('uppercase') ) {
    /**
     * Uppercase
     * 
     * Transforma uma string em maiuscula aplicando a codificação UTF8
     * 
     * @author Gabriel Carvalho <gabriel.dossantoscarvalho@hotmail.com>
     * @param string $str String a ser aumentada
     * @return string
     */
    function uppercase( string $str ): string
    {
        $str = utf8_decode( $str );

        $LATIN_UC_CHARS = utf8_decode( 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ°°ª' );
        $LATIN_LC_CHARS = utf8_decode( 'àáâãäåæçèéêëìíîïðñòóôõöøùúûüý°ºª' );

        $str = strtr( $str, $LATIN_LC_CHARS, $LATIN_UC_CHARS );

        return utf8_encode( strtoupper( $str ) );
    }
}

if ( ! function_exists('lowercase') ) {
    /**
     * Lowercase
     * 
     * Transforma uma string em minuscula aplicando a codificação UTF8
     * 
     * @author Gabriel Carvalho <gabriel.dossantoscarvalho@hotmail.com>
     * @param string $str String a ser diminuida
     * @return string
     */
    function lowercase( string $str ): string
    {
        $str = utf8_decode($str);

        $LATIN_UC_CHARS = utf8_decode( 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ°°ª' );
        $LATIN_LC_CHARS = utf8_decode( 'àáâãäåæçèéêëìíîïðñòóôõöøùúûüý°ºª' );

        $str = strtr ( $str, $LATIN_UC_CHARS, $LATIN_LC_CHARS );

        return utf8_encode( strtolower( $str ) );
    }
}

if ( ! function_exists('lowercaseFirst') ) {
    /**
     * LowercaseFirst
     * 
     * Transforma uma string convertendo apenas a 1ª letra em minuscula
     * 
     * @author Gabriel Carvalho <gabriel.dossantoscarvalho@hotmail.com>
     * @param string $str String a ser aplicada a conversao
     * @return string
     */
    function lowercaseFirst( string $str ): string
    {
        $str = trim($str);

        if( ! isVar( $str ) )
            return $str;

        $str[0] = lowercase( $str[0] );

        return $str;
    }
}

if ( ! function_exists('getEstados') ) {
    /**
     * GetEstados
     * 
     * Retorna os estados do Brasil
     * 
     * Se  $filtro == uf || u          entao retorna  array de Uf dos estados
     * Se  $filtro == estados || e     entao retorna  array de Nome dos estados
     * Se  $filtro == uf de um estado  entao retorna  Nome do estado
     * Se  $filtro == nome do estado   entao retorna  Uf do estado
     *
     * @author Gabriel Carvalho <gabriel.dossantoscarvalho@hotmail.com>
     * @param string $filtro Filtro a ser aplicado no array
     * @return array|string
     */
    function getEstados( string $filtro = '' )
    {
        $estados = array('AC' => 'Acre', 'AL' => 'Alagoas', 'AP' => 'Amapá', 'AM' => 'Amazonas', 'BA' => 'Bahia', 'CE' => 'Ceara', 'DF' => 'Distrito Federal', 'ES' => 'Espirito Santo', 'GO' => 'Goiás', 'MA' => 'Maranhão', 'MT' => 'Mato Grosso', 'MS' => 'Mato Grosso do Sul', 'MG' => 'Minas Gerais', 'PR' => 'Parana', 'PB' => 'Paraiba', 'PA' => 'Para', 'PE' => 'Pernambuco', 'PI' => 'Piaui', 'RJ' => 'Rio de Janeiro', 'RN' => 'Rio Grande do Norte', 'RS' => 'Rio Grande do Sul', 'RO' => 'Rondônia'  , 'RR' => 'Roraima', 'SC' => 'Santa Catarina', 'SE' => 'Sergipe', 'SP' => 'São Paulo', 'TO' => 'Tocantins');

        if( ! isVar( $filtro ) )
            return $estados;

        // apenas UF
        if( $filtro == 'uf' || $filtro == 'UF' || $filtro == 'u' || $filtro == 'U' )
            return array_keys( $estados );

        // apenas Estados
        if( $filtro == 'estados' || $filtro == 'ESTADOS' || $filtro == 'e' || $filtro == 'E' )
            return array_values( $estados );

        $estado;

        // retorna o UF pelo seu nome
        $estado = array_search( str_replace('D', 'd', ucwords( lowercase( $filtro ) ) ) , $estados );

        if( isVar( $estado ) )
            return $estado;

        // retorna o Nome completo do estado pelo seu UF
        $estado = $estados[uppercase( $filtro )];

        if( isVar( $estado ) )
            return $estado;

        return '';
    }
}

if ( ! function_exists('resumeString') ) {
    /**
     * ResumeString
     * 
     * Encurta uma string e(ou) remove as tags HTML
     *
     * @author Gabriel Carvalho <gabriel.dossantoscarvalho@hotmail.com>
     * @param string $string String a ser resumida
     * @param int $lenght Quantidade de caracteres totais da nova string
     * @param string $stringFim String a ser concatenada a string resumida
     * @param bool $removerTags Ativa a remoção das tags da string
     * @return string
     */
    function resumeString( string $string, int $lenght, string $stringFim = '', bool $removerTags = false ): string
    {
        $string = trim($string);

        if( $removerTags )
            $string = strip_tags( $string );

        if( strlen( $string ) <= $lenght )
            return $string;

        $lenght -= strlen( $stringFim );

        $string = substr( $string, 0, $lenght);

        return $string.$stringFim;
    }
}

if ( ! function_exists('isVar') ) {
    /**
     * Verifica se existe uma variavel e se ela nao está vazia
     *
     * @author Gabriel Carvalho <gabriel.dossantoscarvalho@hotmail.com>
     * @param $var Variavel que será verificada
     * @return bool
     */
    function isVar( $var ) : bool
    {
        if( ! isset( $var ) )
            return false;

        if( is_numeric( $var ) )
            return true;

        return ! empty( $var );
    }
}

if ( ! function_exists('getImageAttachement') ) {
    /**
     * Retorna uma imagem (attachement) do post
     * @return array
     */
    function getImageAttachement( $idAttachement, $tamanho = 'full' ): array
    {
        $attachement = get_post( $idAttachement );

        $src = wp_get_attachment_image_src( $idAttachement, $tamanho );

        return array(
            'src'           => $src[0],
            'width'         => $src[1],
            'height'        => $src[2],
            'href'          => get_permalink( $attachement ),
            'title'         => $attachement->post_title,
            'alt'           => get_post_meta( $attachement->ID, '_wp_attachment_image_alt', true ),
            'caption'       => $attachement->post_excerpt,
            'description'   => $attachement->post_content
        );
    }
}