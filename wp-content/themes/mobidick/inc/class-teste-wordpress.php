<?php
/**
 * TesteWordpress Class
 *
 * @author  TesteWordpress
 * @since   1.0
 * @package TesteWordpress
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'TesteWordpress' ) ) :
    /**
     * The main TesteWordpress class
     */
    class TesteWordpress
    {

        /**
         * Setup class.
         *
         * @since 1.0
         */
        public function __construct()
        {
            add_action( 'after_setup_theme',            [ $this, 'setup' ] );
            add_action( 'after_setup_theme',            [ $this, 'register_menus' ] );
            add_action( 'init',                         [ $this, 'add_suport_to_pages' ] );
            add_action( 'wp_enqueue_scripts',           [ $this, 'stylesheets' ], 10 );
            add_action( 'wp_enqueue_scripts',           [ $this, 'scripts' ], 10 );
            add_filter( 'wp_mail_from_name',            [ $this, 'email_from_name' ] );

            /**
             * Remove a meta tag generetor
             */
            remove_action('wp_head',                    'wp_generator');
        }

        /**
         * Define os padrões do tema e registra o suporte para vários recursos do WordPress.
         * 
         * Ocorre em after_setup_theme
         */
        public function setup()
        {
            /**
             * Add default posts and comments RSS feed links to head.
             */
            add_theme_support( 'automatic-feed-links' );

            /*
             * Adiciona o suporte a posts thumbnails
             *
             * @link https://developer.wordpress.org/reference/functions/add_theme_support/#Post_Thumbnails
             */
            add_theme_support( 'post-thumbnails' );

            /**
             * Title tag (support a title-tag [a partir  da versão 4.4])
             */
            add_theme_support('title-tag');

            /**
             * Adicionar custom logo no site
             */
            add_theme_support( 'custom-logo', array(
                'width'         => 108,
                'height'        => 86,
                'flex-heght'    => true,
                'flex-width'    => true,
            ) );

            /*
             * Switch default core markup for search form, comment form, comments, galleries, captions and widgets
             * to output valid HTML5.
             */
            add_theme_support( 'html5', array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'widgets',
            ) );

            // Declare support for selective refreshing of widgets.
            add_theme_support( 'customize-selective-refresh-widgets' );
        }

        /**
         * Registra os menus do tema
         */
        function register_menus()
        {
            register_nav_menus( array(
                'primary'    =>    __( 'Menu Principal', TESTE_WORDPRESS_THEME_NAME ), // Principal
            ) );
        }

        /**
         * Registra e carrega os estilos e fontes do tema
         */
        public function stylesheets()
        {
            //wordpress base
            wp_enqueue_style( 'wordpress_core', TESTE_WORDPRESS_URL_ROOT.'style.css' );

            //Styles
            wp_enqueue_style( 'styles_teste_wordpress', TESTE_WORDPRESS_URL_CSS.'styles.css', array('wordpress_core'), TESTE_WORDPRESS_THEME_VERSION, false );

            //Mobile
            wp_enqueue_style( 'mobile_teste_wordpress', TESTE_WORDPRESS_URL_CSS.'mobile.css', array('styles_teste_wordpress'), TESTE_WORDPRESS_THEME_VERSION, false );
        }

        /**
         * Registra e carrega os scripts do tema
         */
        public function scripts()
        {
            // App
            wp_enqueue_script( 'scripts_teste_wordpress', TESTE_WORDPRESS_URL_JS.'scripts.js', array('jquery'), false, false );

            wp_localize_script( 'scripts_teste_wordpress', 'teste_wordpress_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
        }

        /**
         * Adiciona suporte as páginas
         */
        public function add_suport_to_pages()
        {
            add_post_type_support( 'page', array('excerpt') ); //Adiciona suporte a 'excerpt' nas páginas
        }

        /**
         * Nome do site no envio de e-mail
         */
        function email_from_name( $old ) { return TESTE_WORDPRESS_SITE_NAME; }
    }
endif;

return new TesteWordpress();
