<?php get_header(); ?>

<div class="container d-flex">
    <!-- BEGIN content -->
    <section id="content" class="content">

        <?php while ( have_posts() ) : the_post(); ?>

            <article class="post page" id="pageid-<?php the_ID(); ?>">

                <h1><?php the_title(); ?></h1>

                <?php the_content()?>

            </article>

        <?php endwhile; ?>

    </section> <!-- #content -->
        
    <?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>